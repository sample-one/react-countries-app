import React ,{ useEffect, useState}from 'react'



function Hero() {
 //fetching countries data, set state
 let [countriesInfo,setcountriesInfo] = useState([]);
 
 async function getCountryData(){
 const API_URL = "https://restcountries.com/v3.1/all"
 let response = await fetch(API_URL);
 let countryData = await response.json();


 setcountriesInfo(countryData);
 console.log ("country data are",countryData)



 let countryflag = countryData.map ( (country) =>{
     return country.flags.svg;
     
 })
 console.log("country flags url are",countryflag)
 }

 //using useEffect
 useEffect(  () =>{
  getCountryData();
 },[])



//rendering
  return (
    
    <div className='country-data'>
      <div className='container'>

       {countriesInfo.map((country) => (
        <div className='content'>
          <img src= {country.flags.svg}/>
          <h2>{ country.name.common}</h2>
        </div>
       )
       )  }

      </div>
    </div>
  )
  
}

export default Hero